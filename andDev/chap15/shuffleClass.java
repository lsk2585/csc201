import java.util.List;
import java.util.Arrays;
import java.util.Random;
import java.util.ArrayList;

class shuffleClass {
    public static void main(String args[]) {

        List<String> studArray = new ArrayList<String>();
        List<String> studSorted = new ArrayList<String>();
        List<List> pairs = new ArrayList<List>();

        Random selected = new Random();
        int studPicked;

        String curStud = " ";

        String newStud = "john sam daniel cat william steven";

        curStud = newStud + curStud;
        studArray.addAll(Arrays.asList(curStud.split(" ")));

        List<String> studArrayCopy = new ArrayList<String>(studArray);

        int numPairs = ((studArray.size()) / 2);
        while ((studSorted.size()) != studArray.size()) {
            if (studArrayCopy.size() > 0) {
                studPicked = selected.nextInt(studArrayCopy.size());
                if ((0 <= studPicked) & (studPicked < studArrayCopy.size())) {
                    String remove = studArrayCopy.get(studPicked);
                    studSorted.add(remove);
                    studArrayCopy.remove(studPicked);
                }
            }

        }
        if (studSorted.size() % 2 == 0) {
            int e = 2;
            for (int i = 0; i <= numPairs + 2; i = i + 2) {
                pairs.add(studSorted.subList(i, e));
                System.out.println(pairs);
                e = e + 2;
            }
        }

    }

}
