#include <stdio.h>

int main(){
    int length = 0;
    int c, i;
    int nLength[46];

    for (i = 0; i < 46; ++i)
	nLength[i] = 0;

    while ((c = getchar()) != EOF){	
	if ((c == '.' || c == ',' || c == '"' || c == '/' || c == '-') || (c >=  '0' && c <= '9')){
            continue;
        }
	else if (!(c == ' ' || c == '\n' || c == '\t')){
	    ++length;
	}  
	else{
	    i = length;
	    ++nLength[i-1];
	    length = 0;
	}
     }
	    
    
for(i = 0; i < 46; ++i){
	printf("# of words %d letters long [", i+1);
	
	for (int x = 0; x < nLength[i];  x = x + 50){
	    printf("|");  
	}
	printf("]  %d words\n", nLength[i]);	
    }


}
