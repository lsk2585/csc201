#include <stdio.h>
/* print Celsius to Farenheit table for Celsius-Farenheit table for cels = 0, 20,
...., 300; floating point vrs */

int main(){

printf("Temperature Conversion Program\n");

float fahr, celsius;
int lower, upper, step;

lower = 0; /* lower limit of temp table*/
upper = 300; /* upper lim */
step = 20; /* step size */

celsius = lower;

while (celsius <= upper) {
	fahr = (celsius*(9.0/5.0))+32;
	printf("%3.0f %6.1f\n", celsius, fahr);
	celsius = celsius + step;

}

}
