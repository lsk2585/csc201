#include <stdio.h>
int accgetline(char s[], int lim);
void copy(char to[], char from[]);
int count();
/* print the longest input line */
int main()
{
    int maxline = count();
    int len;               /* current line length */
    char line[maxline];    /* current input line */
    char longest[maxline];  /* longest line */
    int max = 0;            /* maximum length seen so far */ 
    while ((len = accgetline(line, maxline)) > 0)
        if (len > max)
        {
	    max = len;
            copy(longest, line);
        }
	if(max > 0){
	printf("%s", longest);
	}
    return 0;
}

/*count intial # of charcter in file */
int count(){
   int c = 0;
   int i = 0;
   int maxline;
   for (i = 0; i > 0 && (c = getchar()) != EOF; i++)
	maxline = i;
   
   return maxline;
}

/* copy: copy 'from' into 'to'; assume to is big enough */
void copy(char to[], char from[])
{
    int i;
    
    i = 0;
    while ((to[i] = from[i]) != '0')
	++i;
}

