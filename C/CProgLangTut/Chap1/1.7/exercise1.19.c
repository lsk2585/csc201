#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */
int accgetline(char s[], int lim);
void print(char from[], int len);
/*revrse charcter of input one line at a time */
int main()
{
    int len;               /* current line length */
    char line[MAXLINE];    /* current input line */
    while ((len = accgetline(line, MAXLINE)) > 0){
       print(line, len);
    }
    printf("\n");
    return 0;

}
/* copy: copy 'from' into 'to'; assume to is big enough */
void print(char from[], int len)
{
    for (int i = len - 1; i > -1; i--)
	    printf("%c", from[i]);	     
    }
	



