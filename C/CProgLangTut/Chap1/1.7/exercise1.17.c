#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */
int accgetline(char s[], int lim);
void print(char from[]);
/* print the longest input line */
int main()
{
    int len;               /* current line length */
    char line[MAXLINE];    /* current input line */
    while ((len = accgetline(line, MAXLINE)) > 0)
        if (len > 80)
        {
            print(line);
        }
    return 0;
}
/* copy: copy 'from' into 'to'; assume to is big enough */
void print(char from[])
{
    int i;
    i = 0;
    while ((from[i]) != '\0'){
	printf("%c", from[i]);
        ++i;
    }
}
