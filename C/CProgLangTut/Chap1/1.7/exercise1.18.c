#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */
int accgetline(char s[], int lim);
void print(char from[], int final_index);
int final_index(char from[], int len);
/* remove trailing blanks and tabs from lines of input, and deltee entirely balnk lines */
int main()
{
    int len;               /* current line length */
    char line[MAXLINE];    /* current input line */
    while ((len = accgetline(line, MAXLINE)) > 0){
       print(line, final_index(line, len));
    }
    printf("\n");
    return 0;

}
/* copy: copy 'from' into 'to'; assume to is big enough */
void print(char from[], int final_index){
    int i = 0;
    while (i < final_index){
	printf("%c", from[i]);
	i++;
    }
}

int final_index(char from[], int len) {
    char x;
    int i = len-1;
    if ((x = from[i]) != '\t' || x != ' ') 
	return len;    
    for (; i > -1 && from[i] == '\t' && from[i] == ' '; i--)
	return i;
    
         
}

	



