#include <stdio.h>
/* print Farenheti-Celsisu table for Fahrenheit-Celsius table for fahr = 0, 20,
...., 300; floating point vrs */

float conv(int fahr);

int main(){

float fahr, celsius;
int lower, upper, step;
lower = 0; /* lower limit of temp table */
upper = 300; /* upper lim */
step = 20; /* step size */
fahr = lower;

while (fahr <= upper) {
	printf("%3.0f %6.1f\n", fahr, conv(fahr));
	fahr = fahr + step;
	
}

}
float conv(int fahr) {
   float celsius = (5.0/9.0) * (fahr-32);
   return celsius;
}
