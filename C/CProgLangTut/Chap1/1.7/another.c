#include <stdio.h>

int main()
{
    int c;
    int prevchar = 0;

    while ((c = getchar()) != EOF)
    {
        if (c == '\n')   
        {
            continue; //in a new line
        } 
        else
        {
            if (c == ' ' || c == '\t' {
            prevchar++;
            }
            if (prevchar > 1 && 
            putchar(c); //in current line
        }
    }
    printf("\n");

    return 0;
}
