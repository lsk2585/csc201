#include <stdio.h>
#define TRUE 1
#define FALSE 0

int main(){
int c;
char in_white_space = FALSE;
c = getchar();


while ((c=getchar()) != EOF){
    if (c == ' '|| c == '\t' || c == '\n'){
	if (in_white_space) {
	   continue;
	}
	//not in white space
	putchar(' ');
	in_white_space = TRUE;
    }
    else{
	//not a whitespace character
	if (in_white_space){
		in_white_space = FALSE;
    	} 
        putchar(c);	
    }
}



}
