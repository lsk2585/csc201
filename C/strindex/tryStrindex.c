#include <stdio.h>
#include "../lib/acctools.c"
#define MAXLINE 1000
#define SUBSTRING "doink"
#define len 5


int main() {
    char string[MAXLINE];
    char substring[len] = SUBSTRING;
    accgetline(string, MAXLINE);
    printf("getting %s in %s\n", substring, string); 
    printf("%s found at %d\n", substring, strindex(string, substring));

    return 0;
}
