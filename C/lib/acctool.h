/* acctools.h */

/* get line into s, return length */
int accgetline(char s[], int lim);

/* find and return index of t in s, or -1 if not found */
int strindex(char s[], char t[]);
Here is a short program using acctools:

/* line_lengths.c */
#include <stdio.h>
#include "../../lib/acctools.h"

#define MAXLINE 1000   /* maximum input line size */

/* print the length of each input line */
int main()
{
    int len;                           /* current line length */
    char line[MAXLINE];                /* current input line */
    int linenum = 0;                   /* current line number */

    while ((len = accgetline(line, MAXLINE)) > 0) {
        printf("Line %u: %u\n", ++linenum, len);
    }

    return 0;
}
